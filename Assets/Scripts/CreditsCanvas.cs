﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsCanvas : MonoBehaviour {

	[SerializeField] private TextAsset creditsTxtFile;
	[SerializeField] private TMPro.TextMeshProUGUI creditsText;
	// Use this for initialization
	void Start () {
		StartCoroutine(Credits());
	}
	
	IEnumerator Credits()
	{
		string[] lines = creditsTxtFile.text.Split('*');
		while(gameObject)
		{
			for(int i = 0; i < lines.Length; i++)
			{
				creditsText.text = "---Credits---\n" + lines[i];
				yield return new WaitForSeconds(5f);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
