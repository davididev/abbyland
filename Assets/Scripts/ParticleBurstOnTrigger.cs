﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleBurstOnTrigger : MonoBehaviour {

	[SerializeField] private GameObject particlePrefab;
	[SerializeField] private AudioClip soundOnTrigger;
	private GameObject particleInstance;
	
	
	// Use this for initialization
	void Start () {
		particleInstance = GameObject.Instantiate(particlePrefab, transform.position, Quaternion.identity) as GameObject;
		particleInstance.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator ShowParticle(Vector3 pos)
	{
		if(particleInstance.activeSelf == false)
		{
			particleInstance.SetActive(true);
			particleInstance.transform.position = pos;
			AudioSource.PlayClipAtPoint(soundOnTrigger, pos);
			
			ParticleSystem ps = particleInstance.GetComponent<ParticleSystem>();
			ps.Play();
			
			while(ps.isPlaying)
			{
				yield return new WaitForEndOfFrame();
			}
			
			particleInstance.SetActive(false);
		}
		yield return null;
	}
	
	void OnTriggerEnter(Collider col)
	{
		StartCoroutine(ShowParticle(col.transform.position + (Camera.main.transform.forward * 1.5f)));
	}
}
