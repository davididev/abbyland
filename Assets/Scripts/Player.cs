﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public enum GlideSpeed { OUTDOOR_BROAD, OUTDOOR_SLOW, INDOOR_SLOW, INDOOR_BROAD}
	[HideInInspector] public GlideSpeed currentSpeed = GlideSpeed.OUTDOOR_BROAD;
	public Transform camera;
	private Rigidbody rigid;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(rigid == null)
			rigid = GetComponent<Rigidbody>();
		
		
		float s = 5.0f;
		if(currentSpeed == GlideSpeed.OUTDOOR_BROAD)
			s = 5.0f;
		if(currentSpeed == GlideSpeed.OUTDOOR_SLOW)
			s = 2.0f;
		if(currentSpeed == GlideSpeed.INDOOR_BROAD)
			s = 2.0f;
		if(currentSpeed == GlideSpeed.INDOOR_SLOW)
			s = 1.0f;
		Vector3 vel = camera.forward * s;
		
		rigid.velocity = vel;
		
		
		camera.position = transform.position;
	}
}
