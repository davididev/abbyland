﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region : MonoBehaviour {

	public Player.GlideSpeed enterRegion, exitRegion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider col)
	{
		Player p = col.GetComponent<Player>();
		if(p != null)
		{
			p.currentSpeed = enterRegion;
		}
	}
	
	void OnTriggerExit(Collider col)
	{
		Player p = col.GetComponent<Player>();
		if(p != null)
		{
			p.currentSpeed = exitRegion;
		}
	}
}
